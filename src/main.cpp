#include <OneWire.h>
#include <DallasTemperature.h>
#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>

// #define NEED_SERIAL_PRINT

// Dallas configuration
OneWire oneWire(2);
DallasTemperature dallasSensors(&oneWire);

// Wifi configuration
/* =============== config section start =============== */
#if __has_include("credentials.h")
#include "credentials.h"
#else
constexpr const char *const ssid = "Your SSID";
constexpr const char *const password = "Your password";
#endif
/* =============== config section end =============== */
constexpr uint8_t maxWifiReconnectAttempts = 20;

// Web server configuration
ESP8266WebServer server(80);
IPAddress staticIp(192, 168, 0, 221);
IPAddress gateway(192, 168, 0, 1);
IPAddress subnet(255, 255, 255, 0);

// Runtime variables
double actualTemp;
unsigned long boardTime;
bool tempRequested = false;

bool wifiConnect()
{
  if (WiFi.status() == WL_CONNECTED)
    return true;

#ifdef NEED_SERIAL_PRINT
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
#endif

  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  WiFi.config(staticIp, gateway, subnet);

  uint8_t attempts = maxWifiReconnectAttempts;
  while (WiFi.status() != WL_CONNECTED && attempts > 0)
  {
    --attempts;
    delay(500);
#ifdef NEED_SERIAL_PRINT
    Serial.print(".");
#endif
  }

  if (WiFi.status() != WL_CONNECTED)
  {
    return false;
  }

#ifdef NEED_SERIAL_PRINT
  Serial.println();
  Serial.print("WiFi connected. IP address: ");
  Serial.println(WiFi.localIP());
#endif

  return true;
}

void handleRoot()
{
  server.send(200, "text/html", String(actualTemp, 2));
}

void setup()
{
  dallasSensors.begin();

#ifdef NEED_SERIAL_PRINT
  Serial.begin(115200);
#endif

  dallasSensors.requestTemperatures();
  delay(750); // to dallas collect data
  actualTemp = dallasSensors.getTempCByIndex(0);

  wifiConnect();

  server.on("/", handleRoot);
  server.begin();
#ifdef NEED_SERIAL_PRINT
  Serial.println("HTTP server started");
#endif

  boardTime = millis();
}

void loop()
{
  if (!tempRequested)
  {
    dallasSensors.requestTemperatures();
    tempRequested = true;
    boardTime = millis();
  }

  if (millis() - boardTime > 1000)
  {
    actualTemp = dallasSensors.getTempCByIndex(0);
    tempRequested = false;
#ifdef NEED_SERIAL_PRINT
    Serial.print("Current temp: ");
    Serial.println(actualTemp);
#endif
  }

  if (!wifiConnect())
    return;
  server.handleClient();
}
